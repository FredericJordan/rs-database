﻿using System;
using System.Collections.Generic;
using System.Text;
using RSDatabase.Models;

namespace RSDatabase.Methods
{
    static class SensorMethods
    {




        public static float InterpolateConstantBefore(Sensor sensor, DateTime Date)
        {
            if (sensor.Values.Length == 0)
               throw new Exception("Sensor is empty");

            if (Date < sensor.firstTime)
                return sensor.Values[0];

            //if (Date) > sensor.
                
            
            return 0;
        }




        public static RSDatabase.Models.Sensor PasteTimeSerie(Sensor sensor, string strTimeSerie)
        {
            string[] arrayTimeSerie = strTimeSerie.Split("\r\n");
            string[] strFirstTime = arrayTimeSerie[0].Split("\t");
            DateTime firstTime = DateTime.Parse(strFirstTime[0]);

            string[] strLastTime = arrayTimeSerie[arrayTimeSerie.Length - 1].Split("\t");
            DateTime lastTime = DateTime.Parse(strLastTime[0]);

            double averagetimestep = (lastTime - firstTime).TotalSeconds / (arrayTimeSerie.Length - 1);
            int numvalues = (int)((lastTime - firstTime).TotalSeconds / averagetimestep + 1);
            Init(sensor, firstTime, (int)averagetimestep, numvalues);

            //for (int i = 0; i < arrayTimeSerie.Length - 1; i++)
            //{
            //    string[] strTime0 = arrayTimeSerie[i].Split("\t");
            //    string[] strTime1 = arrayTimeSerie[i + 1].Split("\t");
            //    DateTime Time0 = DateTime.Parse(strTime0[0]);
            //    DateTime Time1 = DateTime.Parse(strTime1[0]);

            //    averagetimestep += (Time1 - Time0).TotalSeconds;
            //}

            for (int i = 0; i < arrayTimeSerie.Length; i++)
            {
                string[] strTime = arrayTimeSerie[i].Split("\t");
                float value = float.Parse(strTime[1]);
                sensor.Values[i] = value;
            }

            return sensor;
        }

        private static Sensor Init(Sensor sensor, DateTime firstTime, int timeStep, int numValues)
        {
            System.Array.Resize(ref sensor.Values, numValues);
            sensor.firstTime = firstTime;
            sensor.timeStep = timeStep;
            sensor.numValues = numValues;

            return sensor;
        }
            }
}
