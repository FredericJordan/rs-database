﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using DjdbData.Models;

namespace RSDatabase.Models
{
    class Database
    {
        public Database(string XMLfilePath)
        {
            this.XMLFilePath = XMLfilePath;
            this.ReadXML(XMLfilePath);
        }

        public List<Group> Groups { get; set; } = new List<Group>();

        public string Name { get; set; }

        public string XMLFilePath { get; set; }

        public void ReadXML(string xmlFilePath)
        {
            XmlDocument xd = new XmlDocument();
            xd.Load(xmlFilePath);

            FileInfo FI = new FileInfo(xmlFilePath);
            string databaseFolder = FI.DirectoryName;


            XmlNode datatableNode = xd.SelectSingleNode("/datatable");

            foreach (XmlNode groupNode in datatableNode.ChildNodes)
            {
                if (groupNode.Name == "group")
                {
                    XmlNode groupNameNode = groupNode.SelectSingleNode("groupName");
                    var groupPath = @databaseFolder + "\\" + groupNameNode.InnerText;
                    var group = new Group(groupNameNode.InnerText, groupPath);

                    XmlNode datasetsNode = groupNode.SelectSingleNode("dataSets");
                    foreach (XmlNode datasetNode in datasetsNode.ChildNodes)
                    {
                        if (datasetNode.Name == "dataSet")
                        {
                            XmlNode datasetPathNode = datasetNode.SelectSingleNode("dataSetFileName");
                            var datasetPath = @groupPath + "\\" + datasetPathNode.InnerText;
                            Dataset dataset = new Dataset(datasetPath);
                            XmlNode datasetNameNode = datasetNode.SelectSingleNode("dataSetName");
                            dataset.name = datasetNameNode.InnerText;

                            XmlNode firstDateNode = datasetNode.SelectSingleNode("dataSetFirstDate");
                            dataset.first_date = DateTime.Parse(firstDateNode.InnerText);

                            group.Datasets.Add(dataset);

                        }
                    }
                    this.Groups.Add(group);
                }
            }
        }
    }
}
