﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DjdbData.Models;


namespace RSDatabase.Models
{
    public class Group
    {
        public Group(string Name, string FolderFullPath) 
            {
            this.Name = Name;
            this.FolderfullPath = FolderfullPath;
}

        public string Name { get; set; }
        public string FolderfullPath { get; set; }
        public List<Dataset> Datasets { get; set; } = new List<Dataset>();

        public void RetrieveDatasetsFromFolder(string folderFullPath)
        {
            var listFileInfos = DjdbData.Services.DatasetBinaryIO.GetAllDsBinFileInfosFromFolder(FolderfullPath);

            foreach (var fileinfo in listFileInfos)
            {
                var dataset = new Dataset(fileinfo.FullName);
                Datasets.Add(dataset);
            }
                    }
    }
}
