﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSDatabase.Models
{
    public enum Category
    {
        Precipitation,
        Temperature,
        Discharge,
        Volume
    }


    class Sensor
    {
        public string Description { get; set; }
        public DateTime firstTime { get; set; }
        public int numValues { get; set; }
        public int timeStep { get; set; }
        public Category Category { get; set; }
        public float[] Values = new float[1];



        public Sensor(string Description, DateTime firstTime, int timeStep, int numValues, Category Category)
        {
            this.Description = Description;
            this.firstTime = firstTime;
            this.timeStep = timeStep;
            this.numValues = numValues;
            this.Category = Category;
        }

        public DateTime LastTime(DateTime Date)
        {
            return this.firstTime.AddSeconds((this.numValues-1)*this.timeStep);
        }

        public int IndexLow(DateTime Date)
        {
            if (Date <= this.firstTime)
                return 0;

            if (Date > LastTime(Date))
                return numValues-1;

            double differenceInSeconds = (Date - this.firstTime).TotalSeconds;
            int index = (int)differenceInSeconds / this.timeStep;
            return index;
        }

        public int IndexHigh(DateTime Date)
        {
            if (Date <= this.firstTime)
                return 0;

            if (Date > LastTime(Date))
                return numValues - 1;

            return IndexLow(Date) + 1;
        }


    }



}


