﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSDatabase.Models
{
    class Station
    {
        private string Name;
        private int X;
        private int Y;
        private int Z;
        private List<Sensor> mySensors = new List<Sensor> { };

        public Station(string Name, int X, int Y, int Z)
        {
            this.Name = Name;
            this.X = X;
            this.Y = Y;
            this.Z = Z;
        }

        public void AddSensor(Sensor Sensor)
        {
            mySensors.Add(Sensor);   
    }
    }
    }
